FROM openjdk:8
EXPOSE 9091
ADD target/sbdocker.jar sbdocker.jar
ENTRYPOINT ["java", "-jar", "/sbdocker.jar"]